#!/bin/bash

BUILD=$1
docker system prune -f
docker-compose down -v
docker-compose up $BUILD