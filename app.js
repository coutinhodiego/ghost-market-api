const express = require('express');
const bodyParser = require('body-parser')
const cors = require('cors')
const mongoose = require('mongoose')

const routes = require('./routes/routes')

const app = express();

const PORT = process.env.PORT || 3000
const MONGO_DB = process.env.MONGO_DB

app.use(bodyParser.urlencoded({
    extended: true,
    limit: '50mb'
}))
app.use(bodyParser.json())
app.use(cors());

app.use('/', routes)

//Open connection
mongoose.connect(MONGO_DB, { useNewUrlParser: true })
.then((result) => {

    //Abrindo conexao da aplicaçao
    const server = app.listen(PORT, () => {
        let host = result.connections[0].host

        console.log(`DATA-BASE : ${host}`)
        console.log(`LISTEN ON PORT : ${PORT}`)
        console.log('API - ONLINE')
    });

    const io = require('./socket').init(server)
    io.on('connection', socket => {
        console.log('Socket.io client connected.')
    })
})
.catch(err => console.log(err))
