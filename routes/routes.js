const express = require('express')
const router = express.Router()

const productController = require('../controllers/productController')
const orderController = require('../controllers/orderController')

//Rotas de Product.js
router.post('/v1/product/save', productController.saveProduct)
router.get('/v1/product/list', productController.listProduct)
router.get('/v1/product/detail/:idProduct', productController.detailProduct)
router.put('/v1/product/update', productController.updateProduct)


//Rotas de Order.js
router.post('/v1/order/save', orderController.saveOrder)
router.put('/v1/order/pay', orderController.payOrder)
router.put('/v1/order/deliver', orderController.deliverOrder)
router.get('/v1/order/list/:gaveta', orderController.listOrder)
router.get('/v1/order/detail/:idOrder', orderController.detailOrder)


module.exports = router;