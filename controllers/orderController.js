const Product = require('../models/Product')
const Order = require('../models/Order')

const io = require('../socket')

exports.saveOrder = async (req, res) => {

    let o = req.body
    if (o.product == undefined || o.gaveta == undefined || o.product == "" || o.gaveta == "") {
        return res.status(400).json({ code: 400, message: "Requisição com campos  inválidos", detail: "product e gaveta são obrigatórios" })
    }

    let orderdb = new Order({
        paid: false,
        delivered: false,
        gaveta: o.gaveta,
        product: o.product
    })


    try {
        let order = await orderdb.save()

        return res.status(201).json({
            code: 201,
            message: "Order criada com sucesso",
            data: order
        })

    } catch (err) {
        return res.status(400).json({
            code: 400,
            message: "Erro ao criar uma order",
            detail: err.message
        })
    }
};

exports.payOrder = async (req, res) => {

    let o = req.body
    if (o.order == undefined || o.order == "") {
        return res.status(400).json({ code: 400, message: "Requisição com campos inválidos", detail: "order é um campo obrigatório" })
    }

    try {
        let order = await Order.findById(o.order)

        if (!order) return res.status(400).json({ code: 400, message: "Erro ao recuperar uma order", detail: "esta ordem não foi encontrada" })

        let orderpaid = await Order.findByIdAndUpdate(o.order, { paid: true }, { new: true })

        if (orderpaid) {
            let products = await Order.find({ gaveta: order.gaveta, paid: true, delivered: false }).populate('product')
            io.getIo().emit('gate', { code: 200, gaveta: order.gaveta, data: products })
            // io.getIo().emit(`gate${order.gaveta}`, { code: 200, message: "Order paga", data: products })
        }


        return res.status(200).json({
            code: 200,
            message: "Order paga com sucesso",
            data: orderpaid
        })

    } catch (err) {
        return res.status(400).json({
            code: 400,
            message: "Erro ao recuperar uma order",
            detail: err.message
        })
    }
};

exports.deliverOrder = async (req, res) => {

    let o = req.body
    if (o.order == undefined || o.order == "") {
        return res.status(400).json({ code: 400, message: "Requisição com campos inválidos", detail: "order é um campo obrigatório" })
    }

    try {

        let order = await Order.findOne({ _id: o.order }).populate('product')

        let qtdNova = order.product.qtd

        if (order.product.qtd <= 1) {
            qtdNova = 0
        } else {
            qtdNova = order.product.qtd - 1
        }

        let orderpaid = await Order.findByIdAndUpdate(order._id, { delivered: true }, { new: true })
        let product = await Product.findByIdAndUpdate(order.product._id, { qtd: qtdNova }, { new: true })

        console.log(product)

        return res.status(200).json({
            code: 200,
            message: "Order entregue com sucesso",
            data: orderpaid
        })

    } catch (err) {
        return res.status(400).json({
            code: 400,
            message: "Erro ao recuperar uma order",
            detail: err.message
        })
    }
};

exports.listOrder = async (req, res) => {

    try {
        let orders = await Order.find({ gaveta: req.params.gaveta, paid: true, delivered: false }).populate('product')

        return res.status(200).json({
            code: 200,
            message: "Pedidos Atualizados",
            data: orders
        })

    } catch (err) {
        return res.status(400).json({
            code: 400,
            message: "Erro ao recuperar orders",
            detail: err.message
        })
    }
}

exports.detailOrder = async (req, res) => {

    try {
        let order = await Order.findOne({ _id: req.params.idOrder }).populate('product')

        return res.status(200).json({
            code: 200,
            message: "Pedido Recuperado",
            data: order
        })

    } catch (err) {
        return res.status(400).json({
            code: 400,
            message: "Erro ao recuperar a order",
            detail: err.message
        })
    }
}