
const Product = require('../models/Product')

exports.saveProduct = async (req, res) => {

    let p = req.body
    if (p.name == undefined || p.description == undefined || p.price == undefined || p.image == undefined || p.qtd == undefined ||
        p.name == "" || p.description == "" || p.price == "" || p.image == "") {
        return res.status(400).json({ code: 400, message: "Requisição com campos  inválidos", detail: "name, description, price, image e qtd são obrigatórios" })
    }

    let productdb = new Product(req.body)

    try {
        let product = await productdb.save()
        return res.status(201).json({
            code: 201,
            message: "Produto criado com sucesso",
            data: product
        })

    } catch (err) {
        return res.status(400).json({
            code: 400,
            message: "Erro ao criar produto",
            detail: err.message
        })
    }
};


exports.updateProduct = async (req, res) => {

    let p = req.body
    if (p.name == undefined || p.description == undefined || p.price == undefined || p.image == undefined || p.qtd == undefined ||
        p.name == "" || p.description == "" || p.price == "" || p.image == "") {
        return res.status(400).json({ code: 400, message: "Requisição com campos  inválidos", detail: "name, description, price, image e qtd são obrigatórios" })
    }

    try {
        let product = await Product.findByIdAndUpdate(p.id)
        return res.status(201).json({
            code: 201,
            message: "Produto atualizado com sucesso",
            data: product
        })

    } catch (err) {
        return res.status(400).json({
            code: 400,
            message: "Erro ao criar produto",
            detail: err.message
        })
    }
};


exports.listProduct = async (req, res) => {

    try {
        let products = await Product.find({ $or: [{ qtd: { $ne: 0 } }] })
        return res.status(200).json({
            code: 200,
            message: "Produtos listados",
            data: products
        })

    } catch (err) {
        return res.status(400).json({
            code: 400,
            message: "Erro ao criar produto",
            detail: err.message
        })
    }

}

exports.detailProduct = async (req, res) => {

    try {
        let product = await Product.findOne({ _id: req.params.idProduct })

        if (!product) return res.status(400).json({ code: 400, message: "Erro ao recuperar o produto", detail: "Este produto não foi encontrado" })

        return res.status(200).json({
            code: 200,
            message: "Produto Recuperado",
            data: product
        })

    } catch (err) {
        return res.status(400).json({ 
            code: 400, 
            message: "Erro ao recuperar o produto", 
            detail: err.message 
        })
    }
}
