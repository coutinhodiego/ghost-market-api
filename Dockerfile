FROM node:alpine

COPY . /node/api

EXPOSE $PORT

WORKDIR /node/api

RUN npm install

CMD ["npm", "start"]