require('mongoose-bigdecimal');

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Order = new Schema({
    paid:{ type: Boolean, required: true },
    delivered: {type: Boolean, required: true},
    gaveta:{ type: String, required: true },
    product:{ type: Schema.Types.ObjectId, ref: 'Product', required: true}
})

module.exports = mongoose.model('Order', Order);
